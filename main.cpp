// oop-lab1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>

#ifdef _WIN32
#include <Windows.h>
#endif // WIN32

struct Person
{
    struct Address
    {
        std::string street;
        int zip;
        std::string city;

        // Constructor for Address struct
        Address() {}
        Address(std::string address)
        {
            size_t streetbegin = address.find_first_of(',');
            street = address.substr(0, streetbegin);
            zip = std::stoi(address.substr(streetbegin + 2, 6).erase(3, 1));

            size_t citybegin = streetbegin + 10;
            size_t citycount = address.find("   ", citybegin) - citybegin;
            city = address.substr(citybegin, citycount);
        }
    };

    std::string name;
    std::string id;
    Address location;

    // Constructor for Person struct
    Person() {}
    Person(std::string name, std::string id, std::string address)
        : name(name), id(id), location(address)
    {}
};

std::istream& operator>> (std::istream& in, Person& p);
std::vector<Person> read_file(std::string filename);
size_t find_in_names(std::vector<Person> persons, std::string name_part);
std::vector<Person> find_person_from_city(std::vector<Person> haystack, std::string city);

int main()
{
    // Skriva ut svenska bokst�ver i konsolen
#ifdef _WIN32
    SetConsoleOutputCP(65001);
#endif // _WIN32

    std::vector<Person> persons = read_file("names.txt");

    while (true)
    {
        std::cout << std::endl
            << "Select an action:\n"
            << "1 - Search people by partial name" << std::endl
            << "2 - Search people by city" << std::endl
            << "3 - Quit" << std::endl << std::endl;

        int selection = 0;
        std::cin >> selection;
        std::cin.clear();
        std::cin.ignore(10000, '\n');

        switch (selection)
        {
        case 1:
        {
            std::cout << "Enter a name to search for:" << std::endl;

            std::string nametofind;
            std::getline(std::cin, nametofind);
            size_t results = find_in_names(persons, nametofind);
            // Alternativ 1 ska endast skriva ut antalet funna namn p� en egen linje, d�refter visa menyn igen. [heltal+nyrad].
            std::cout << results << std::endl;

            break;
        }
        case 2:
        {

            std::cout << "Enter a city to filter people by:" << std::endl;

            // Read the city the user wants to search by
            std::string cityfilter;
            std::getline(std::cin, cityfilter);

            auto results = find_person_from_city(persons, cityfilter);

            // for-range loop
            for (auto& person : results)
            {
                // Alternativ 2 ska endast skriva ut personens namn och ortnamnet p� en rad, d�refter visa menyn. [namn, ortnamn+nyrad]. 
                std::cout << person.name << ", " << person.location.city << std::endl;
            }

            break;
        }
        case 3:
        {
            return 0;
            break;
        }
        default:
        {
            std::cout << "Ok�nt val" << std::endl;
            break;
        }
        }
    }

    return 0;
}

std::istream& operator>> (std::istream& in, Person& p)
{
    std::string name, id, address;

    std::getline(in, name);
    if (name == "") return in; // If we've reached the bottom

    std::getline(in, id);
    std::getline(in, address);

    p = Person(name, id, address);
    return in;
}

/**
    * Read from file <filename> with the format
    * Name\n
    * ID\n
    * Street data, 5 digit zip[2 SPACE]city\n
    * @returns vector<person>. If file is not found, it returns an empty vector.
    */
std::vector<Person> read_file(std::string filename)
{
    std::ifstream file(filename);
    std::vector<Person> vec;

    if (file.good())
    {
        while (!file.eof())
        {
            Person person;
            file >> person;
            vec.push_back(person);
        }

        std::cout << "Read all users from " << filename << std::endl;
    }
    else {
        std::cout << "Could not read '" << filename << "', no persons retrieved" << std::endl;
    }

    return vec;
}

/**
    * Look in vector for names containing the substring name_part.
    * The search is case insensitive.
    * @returns the number of occurences name_part is found.
    */
size_t find_in_names(std::vector<Person> persons, std::string name_part)
{
    std::string nametofind = name_part;
    std::transform(nametofind.begin(), nametofind.end(), nametofind.begin(), tolower);

    size_t results = 0;
    for (auto& person : persons)
    {
        auto name = person.name;
        std::transform(name.begin(), name.end(), name.begin(), tolower);
        if (name.find(nametofind) != std::string::npos)
        {
            results++;
        }
    }

    return results;
}

/**
    * Look in vector for persons living in a particular city.
    * Exact matches only.
    * The search is case insensitive.
    * @returns a vector containg the matched persons.
    */
std::vector<Person> find_person_from_city(std::vector<Person> haystack, std::string city)
{
    // Change to lowercase
    std::string cityfilter = city;
    std::transform(cityfilter.begin(), cityfilter.end(), cityfilter.begin(), tolower);

    // Loop all persons
    std::vector<Person> results;
    for (auto& person : haystack)
    {
        // Get city of the current person
        auto city = person.location.city;
        std::transform(city.begin(), city.end(), city.begin(), tolower); // To lowercase

        // Check if the city matches the search query
        if (city == cityfilter)
        {
            results.push_back(person);
        }
    }

    return results;
}

